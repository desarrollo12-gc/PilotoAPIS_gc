package com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.grupocubo.desarrollo12.pilotoapis_gc.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class PicassoActivity extends AppCompatActivity{

    ImageView imageView;
    int i = 0;
    Button btnDrawableImage, btnUrlImage, btnErrorImage, btnPlaceholderImage, btnCallback, btnResizeImage, btnRotateImage, btnScaleImage, btnTarget;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picasso);
        initView();

    }

    /////--------------------------------------------------------------------

    private void initView() {

        imageView = (ImageView) findViewById(R.id.imageView);
        btnDrawableImage = (Button) findViewById(R.id.btnDrawable);
        btnUrlImage = (Button) findViewById(R.id.btnUrl);
        btnPlaceholderImage = (Button) findViewById(R.id.btnPlaceholder);
        btnErrorImage = (Button) findViewById(R.id.btnError);
        btnCallback = (Button) findViewById(R.id.btnCallBack);
        btnResizeImage = (Button) findViewById(R.id.btnResize);
        btnRotateImage = (Button) findViewById(R.id.btnRotate);
        btnScaleImage = (Button) findViewById(R.id.btnScale);
        btnTarget = (Button) findViewById(R.id.btnTarget);

        btnDrawableImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Picasso.get().load(R.drawable.image).into(imageView);
                System.out.println("entro acá");
            }
        });

        btnUrlImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.get().load("http://www.grupo-cubo.com/img/logo.png").placeholder(R.drawable.placeholder).into(imageView);
            }
        });

        btnPlaceholderImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.get().load("www.journaldev.com").placeholder(R.drawable.placeholder).into(imageView);
            }
        });

        btnErrorImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.get().load("www.journaldev.com").placeholder(R.drawable.placeholder).error(R.drawable.error).into(imageView);
            }
        });

        btnCallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Picasso.get().load("www.journaldev.com").error(R.mipmap.ic_launcher).into(imageView, new Callback.EmptyCallback() {
                    @Override
                    public void onSuccess() {
                        Log.d("TAG", "onSuccess");
                    }


                    public void onError() {
                        Toast.makeText(getApplicationContext(), "An error occurred", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        btnResizeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.get().load(R.drawable.image).resize(200, 200).into(imageView);
            }
        });

        btnRotateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.get().load(R.drawable.image).rotate(90f).into(imageView);
            }
        });

        btnScaleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i == 3)
                    i = 0;

                else {
                    if (i == 0) {
                        Picasso.get().load(R.drawable.image).fit().into(imageView);
                        Toast.makeText(getApplicationContext(), "Fit", Toast.LENGTH_SHORT).show();
                    } else if (i == 1) {
                        Picasso.get().load(R.drawable.image).resize(200, 200).centerCrop().into(imageView);
                        Toast.makeText(getApplicationContext(), "Center Crop", Toast.LENGTH_SHORT).show();
                    } else if (i == 2) {
                        Picasso.get().load(R.drawable.image).resize(200, 200).centerInside().into(imageView);
                        Toast.makeText(getApplicationContext(), "Center Inside", Toast.LENGTH_SHORT).show();
                    }
                    i++;
                }
            }
        });

        btnTarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.get().load("http://cdn.journaldev.com/wp-content/uploads/2017/01/android-constraint-layout-sdk-tool-install.png").placeholder(R.drawable.placeholder).error(R.drawable.error).into(target);
            }
        });
    }


     Target target= new Target() {
         @Override
         public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
             imageView.setImageBitmap(bitmap);
         }

         @Override
         public void onBitmapFailed(Exception e, Drawable errorDrawable) {
             imageView.setImageDrawable(errorDrawable);
         }

         @Override
         public void onPrepareLoad(Drawable placeHolderDrawable) {
             imageView.setImageDrawable(placeHolderDrawable);
         }
     };
    ////---------------------------------------------------------------------
}
