package com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.grupocubo.desarrollo12.pilotoapis_gc.Classes.Recursiva;
import com.grupocubo.desarrollo12.pilotoapis_gc.R;
import com.grupocubo.desarrollo12.pilotoapis_gc.Widget.OnSwipeTouchListener;


public class FormularioActivity_cg5 extends AppCompatActivity {


    private Integer cedula;
    private LinearLayout linearLayout_Formulario_cg5;
    private Spinner spinner_final;
    private Button btn_volver5;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_cg5);

        spinner_final = (Spinner) findViewById(R.id.spinner_finalidad);

        /*-----------------------------------
        Recibir datos del activity anterior
        ///----------------------------------*/

        Intent intent=getIntent();
        Bundle extras =intent.getExtras();
        if (extras != null) {//ver si contiene datos
            Integer cedulaRecibida =(Integer) extras.get("cedula");
            cedula = cedulaRecibida;
            //Toast.makeText(FormularioActivity_cg5.this, "Cedula: " + String.valueOf(cedula), Toast.LENGTH_SHORT).show();
            //consulta(cedulaRecibida);
        }

        linearLayout_Formulario_cg5 = (LinearLayout) findViewById(R.id.linearLayout_formulario_cg5);

        linearLayout_Formulario_cg5.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg5.this) {

            public void onSwipeRight() {
                //Toast.makeText(FormularioActivity_cg5.this, "right", Toast.LENGTH_SHORT).show();

                Intent ListSong = new Intent(FormularioActivity_cg5.this, FormularioActivity_cg4.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);

            }

        });

        btn_volver5 = (Button) findViewById(R.id.btn_volver5);

        btn_volver5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent ListSong = new Intent(FormularioActivity_cg5.this, FormularioActivity_cg0.class);
                startActivity(ListSong);
            }

        });

    }

    public void guardarDB5(View v) {
       Recursiva re = new Recursiva();
       re.guardarDB(getBaseContext(),cedula);
    }



}
